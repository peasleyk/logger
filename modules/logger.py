import os,sys, time
import datetime, time
import configparser
"""
  A simplified custom logging module
  I thought it would be fun to write my own that includes color
  does not contain handlers or formatters as the default one does,
  just simple logging

  Configuration files and instantiating are probably very different between the two
  (i.e non compatible to simply replace the import )

  TODO:
    highlighting
    possibly add formatting (This is supposed to be a simplified logger,
                              and this may be out of that scope)
    exceptions - high
    fix date/time right bracket
    add more tests
"""

class logger():

  __formats = {
    'HEADER' :'\033[95m',
    'BLUE'   :'\033[94m',
    'GREEN'  : '\033[92m',
    'ORANGE' : '\033[93m',
    'RED'    : '\033[91m',
    'ENDC'   : '\033[0m',
    'BOLD'  : "\033[1m",
  }

  __levels = {
    'EXTREME'   : 30,
    'EXCEPTION' : 0,
    'WARNING'   : 0,
    'DEBUG'     : 20,
    'INFO'      : 10,
    'ERROR'     : 0,
  }

  def __init__(self,color=None, file=None, level='INFO', highlight=False,
              sformat=None, time=None, date=None, bold = None,
              surppress=False, createLog=False, config=None, argdict=None, **kwargs):

    options = {
      'color' : color,
      'file'  : file,
      'level' : level,
      'highlight' : highlight,
      'date' : date,
      'time' : time,
      'sformat' : sformat,
      'bold' : bold,
      'suppress' : surppress,
      'createLog' : createLog,
      'config' : config
          }


    def loadFromConfig(config):
      """
        This method will read the config.ini, updating options[] with
        defines values
      """
      def exists(parser,option, default):
        """
          This checks for existence of a key in config.ini and returns it id exists
        """
        if parser.has_option('logger', option):
          return parser.get('logger',option)
        else:
          return default

      parser = configparser.ConfigParser(allow_no_value=True)
      try:
        parser.read(config)
      except:
        self.__sendLog('ERROR', 'Cannot access {}'.format(config))
        return

      for keys, values in options.items():
        options[keys] = exists(parser,keys,values)

    #Allows a dict to be passed in
    if argdict:
      options = {**options, **argdict}
      self.argdict = True
    else:
      self.argdict = False
    #config file overrides passed in arguments
    if config:
      loadFromConfig(config)
      self.config = True
    else:
      self.config = False

    self.color = options['color']
    self.file = options['file']
    self.createLog = options['createLog']
    self.level = options['level']
    self.highlight = options['highlight']
    self.date = options['date']
    self.time = options['time']
    self.sformat = options['sformat']
    self.bold = options['bold']
    self.suppress = options['suppress']



    if self.createLog:
      if self.file:
        open(self.file,'w+').close
      else:
        self.file = "logging.log"
        open(self.file, 'w+').close

    return



  def opts(self):
    """
        useful to debug which options are selected
    """
    print('Selected options')
    print('Mode: {}'.format(self.level))
    print('Logfile location: {}'.format(self.file))
    print('Color: {}'.format(self.color))
    print('Date: {}'.format(self.date))
    print('Time: {}'.format(self.time))
    print('Highlight: {}'.format(self.highlight))
    print('Bold: {}'.format(self.bold))
    if self.config:
      print('config file: True')
    else:
      print ('Config File: False')
    if self.argdict:
      print('Argdict: True')
    else:
      print('Argdict: False')



  def __writeToFile(self,message):
    """
        Internal method used to write logging to a file
    """

    if os.path.exists(self.file):
      with open(self.file, 'a') as logfile:
        logfile.write('{}\n'.format(message))
    else:
      #Bypass log methods and skip recursion error
      self.__sendLog("ERROR", "Cannot access {}".format(self.file), True)
      return
    return

  """
      These define each level, and what they send
      Mostly just the level and message

      Exception allows you to pass in the exception as an optional argument,
      or you can format it yourself
  """

  def debug(self, message):
    self.__sendLog('DEBUG',message)

  def info(self, message):
    self.__sendLog('INFO', message)

  def error(self,message):
    self.__sendLog('ERROR', message)

  def warning(self,message):
    self.__sendLog('WARNING', message)
  def extreme(self,message):
    """
      Used to send very detailed information , like full html, xml, etc
    """

    self.__sendLog('EXTREME', message)

  def exception(self,message,exception=None):
    if exception:
      self.__sendLog('EXCEPTION','{}: {}'.format(message,exception))
    else:
      self.__sendLog('EXCEPTION', message)


  def __sendLog(self,loglevel,message,skip=None):
    """
      This takes in a log level and a message
      It compares the log level given to the log level chosen to decide what to send
      Formatting (bold, color, time, date, etc) is handled by separate functions
    """
    if not self.level:
      return

    #By splitting up between the message and debug statement, we can more easily
    #edit them to include color, highlighting, etc down the line
    toPrint = ''
    level = loglevel
    colorlevel = loglevel

    if self.__levels[self.level] >= self.__levels[loglevel]:
      if self.bold:
        level = self.__getBold(colorlevel)

      if self.highlight:
        pass
        level = self.__getHighlight(colorlevel)

      if self.color:
        if loglevel == 'EXCEPTION' or loglevel == 'ERROR':
          level = self.__getColor('RED',colorlevel)
        if loglevel == 'DEBUG':
          level = self.__getColor('BLUE',colorlevel)
        if loglevel == 'WARNING':
          level = self.__getColor('ORANGE',colorlevel)
        if loglevel == 'INFO':
          level = self.__getColor('GREEN',colorlevel)
        if loglevel == 'EXTREME':
          level = self.__getColor('BLUE',colorlevel)

      if self.time:
        timeNow =  self.__getTime()
        toPrint = '{} {}'.format(timeNow,toPrint)

      if self.date:
        dateNow = self.__getDate()
        toPrint = '{} {}'.format(dateNow,toPrint)

      if self.time or self.date:
        toPrint = '[{}] '.format(toPrint)

    else:
      return

    #This is needed as ansi escape codes show up when logging the string
    if self.file and not skip:
      toPrint = '{}{}: {}'.format(toPrint, colorlevel, message)
      self.__writeToFile(toPrint)
    else:
      toPrint = '{}{}: {}'.format(toPrint, level, message)
      print(toPrint)


  """
    These all handle applying chosen formatting to different parts of the message
  """
  def __getDate(self, dFormat=''):
    if dFormat:
      currDate = time.strftime(dFormat)
    else:
      currDate = time.strftime('%Y-%m-%d')
    return currDate

  def __getTime(self,tFormat=''):
    if tFormat:
      currTime = time.strftime(tFormat)
    else:
      currTime = time.strftime('%H:%M:%S')
    return currTime

  def __getBold(self,message):
    message = '{}{}{}'.format(self.__formats['BOLD'], message, self.__formats['ENDC'])
    return message

  def __getHighlight(self,message):
    return

  def __getColor(self,color,message):
    message = '{}{}{}'.format(self.__formats[color], message ,self.__formats['ENDC'])
    return message

