import os, sys, unittest
import datetime, time

#This adds the current dir to the path
sys.path.append( os.path.dirname( os.path.dirname( os.path.abspath(__file__) ) ))

from modules.logger import logger

class basicOutput(unittest.TestCase):
  def setup(self):
    pass


  def testInfo(self):
    log = logger(level="DEBUG")
    log.info('test')
    output = sys.stdout.getvalue().strip()
    self.assertEqual(output, 'INFO: test')

  def testDebug(self):
    log = logger(level="DEBUG")
    log.info('test')
    output = sys.stdout.getvalue().strip()
    self.assertEqual(output, 'INFO: test')

  def testError(self):
    log = logger(level="DEBUG")
    log.error('test')
    output = sys.stdout.getvalue().strip()
    self.assertEqual(output, 'ERROR: test')

  def testException(self):
    log = logger(level="INFO")
    log.exception('test','error message')
    output = sys.stdout.getvalue().strip()
    self.assertEqual(output, 'EXCEPTION: test: error message')

  def testExceptionWithE(self):
    log = logger(level="INFO")
    log.exception('whoops')
    output = sys.stdout.getvalue().strip()
    self.assertEqual(output, 'EXCEPTION: whoops')

  def testignoreDebug(self):
    log = logger(level="INFO")
    log.debug('test')
    output = sys.stdout.getvalue().strip()
    self.assertEqual(output, '')

  def testExtreme(self):
    log = logger(level="EXTREME")
    log.extreme('test')
    output = sys.stdout.getvalue().strip()
    self.assertEqual(output, 'EXTREME: test')

  def testExtreme2(self):
    log = logger(level="INFO")
    log.extreme('test')
    output = sys.stdout.getvalue().strip()
    self.assertEqual(output, '')


class textformat(unittest.TestCase):
    def setup(self):
      pass

    def testInfoColor(self):
      log = logger(level="INFO", color=True)
      log.info('test')
      output = sys.stdout.getvalue().strip()
      self.assertEqual(output, '\x1b[92mINFO\x1b[0m: test')

    def testDebugColor(self):
      log = logger(level="DEBUG", color=True)
      log.debug('test')
      output = sys.stdout.getvalue().strip()
      self.assertEqual(output, '\x1b[94mDEBUG\x1b[0m: test' )

    def testWarnColor(self):
      log = logger(level="WARNING", color=True)
      log.warning('test')
      output = sys.stdout.getvalue().strip()
      self.assertEqual(output, '\x1b[93mWARNING\x1b[0m: test')

    def testErrorColor(self):
      log = logger(level="ERROR", color=True)
      log.error('test')
      output = sys.stdout.getvalue().strip()
      self.assertEqual(output, '\x1b[91mERROR\x1b[0m: test' )

    def testExceptionColor(self):
      log = logger(level="EXCEPTION", color=True)
      log.exception('test')
      output = sys.stdout.getvalue().strip()
      self.assertEqual(output, '\x1b[91mEXCEPTION\x1b[0m: test')

    def testExtremeColor(self):
      log = logger(level="EXTREME", color=True)
      log.extreme('test')
      output = sys.stdout.getvalue().strip()
      self.assertEqual(output, '\x1b[94mEXTREME\x1b[0m: test')

    def testBold(self):
      log = logger(level="INFO", bold=True)
      log.info('test')
      output = sys.stdout.getvalue().strip()
      self.assertEqual(output, '\x1b[1mINFO\x1b[0m: test')


    def testcolorBold(self):
      log = logger(level="INFO", color=True, bold=True)
      log.info('test')
      output = sys.stdout.getvalue().strip()
      self.assertEqual(output, '\x1b[92mINFO\x1b[0m: test')

class timeandDate(unittest.TestCase):
  """
    TODO
    -reduce length of tests with wrapper
  """

  def setup(self):
    pass

  def getDate(self, dFormat=''):
    if dFormat:
      currDate = time.strftime(dFormat)
    else:
      currDate = time.strftime('%Y-%m-%d')
    return currDate

  def getTime(self,tFormat=''):
    if tFormat:
      currTime = time.strftime(tFormat)
    else:
      currTime = time.strftime('%H:%M:%S')
    return currTime

  def testTime(self):
    log = logger(level="INFO", time=True)
    log.info('test')
    output = sys.stdout.getvalue().strip()
    time = self.getTime()
    final = '[{} ] INFO: test'.format(time)
    self.assertEqual(output, final )

  def testColorTime(self):
    log = logger(level="INFO", time=True, color=True)
    log.info('test')
    output = sys.stdout.getvalue().strip()
    time = self.getTime()
    final = '[{} ] \x1b[92mINFO\x1b[0m: test'.format(time)
    self.assertEqual(output, final )

  def testDate(self):
    log = logger(level="INFO", date=True)
    log.info('test')
    output = sys.stdout.getvalue().strip()
    date = self.getDate()
    final = '[{} ] INFO: test'.format(date)
    self.assertEqual(output, final )


  def testColorDate(self):
    log = logger(level="INFO", date=True, color=True)
    log.info('test')
    output = sys.stdout.getvalue().strip()
    date = self.getDate()
    final = '[{} ] \x1b[92mINFO\x1b[0m: test'.format(date)
    self.assertEqual(output, final )


  def testDateandTime(self):
    log = logger(level="INFO", date=True, time=True)
    log.info('test')
    output = sys.stdout.getvalue().strip()
    date = self.getDate()
    time = self.getTime()
    final = '[{} {} ] INFO: test'.format(date,time)
    self.assertEqual(output, final )


  def testColorDateAndTime(self):
    log = logger(level="INFO", date=True, time=True, color=True)
    log.info('test')
    output = sys.stdout.getvalue().strip()
    date = self.getDate()
    time = self.getTime()
    final = '[{} {} ] \x1b[92mINFO\x1b[0m: test'.format(date,time)
    self.assertEqual(output, final )










if __name__ == "__main__":
    try:
        unittest.main(buffer=True)
    except KeyboardInterrupt:
        pass
    finally:
        sys.exit

