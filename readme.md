
Overview
=========
A colorful logger for python
This is just a fun side thing to create a python logger from scratch
It has color, multiple configuration options, multiple ways to config



The following functionality is provided:
+ levels
+ color
+ bold
+ date
+ time
+ log to a file
+ generate a file to log to

Using
=====

To use, place logger in the modules/ directorty of your project, and import it
```
from modules.logger import logger
```

Here is a sample set up
```
log = logger(level="DEBUG")
log.info('test')
```
This will output
```
INFO: test
```
logger also can take in an arg dict
```
args={
    level='INFO'
}
log = logger(argDict=args)
```
and also read from an config file
```
log = logger(config='my_config.ini)
```
and in my_config.ini,
```
[logger]
level=
```

Configuration options
=====================
These configuration options can be passed in though the constructor, an arg dict, or a config.ini
An arg dict will overrite constructor options, and a config file will overide an arg dict

+ color: True or False
+ bold: True or False
+ level: INFO, DEBUG, ERROR, EXCEPTION, WARNING,EXTREME
+ date: True or False
+ time: True or False
+ config: 'config/location/config.ini'
+ createLog: True or False
+ argdit: my_arg_dict
+ ile: 'my/logfile/location.log'

These oiptions __are__ case sensative

Color
-----
Each level has it's own color

+ INFO: Green
+ DEBUG: Blue
+ WARNING: Orange
+ EXCEPTION: Red
+ ERROR: Red
+ EXTREME: green

bold
----
Setting this to true will bold each color, or bold white if none is set

level
-----


How each log level is represented:
+ WARNING: 0
+ EXCEPTION: 0
+ ERROR: 0
+ INFO: 10
+ DEBUG 20
+ EXTREME: 30

Extreme is used for very detailed output (full xml, html, etc) base don my needs

date
----

Setting this to true will format like so
```
[2012-01-22] INFO: message
```


time
----

Setting this to true will format like so
```
[22:23:31 ] INFO: message
```

Combining elements
-------------------

Every option should be able to be combined
For example, you can have both date, time, color, and a bolded message



Configuration
---------------

You can either create:

+ a dictionary with keys and values mentioned above
+ pass in the options and values in the cunstructor, eg log = new logger(color=True, bold=True)
+ create a config.ini like the one in this repository using the options and values above
